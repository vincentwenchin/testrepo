/**
 * @author	YuRen
 * @version 1.0.0
 * Last Change: 2016/07/04 Mon 2:55 PM
 */
public class TestProject {
	public static void main(String[] args) {
		System.out.println("hello\n");
		sayHi();
	}

	private static void sayHi() {
		System.out.println("hi");
	}
}
